#include "src/pi_fm_rds.h"
#include <assert.h>

// A semi-useless test for pi_fm_rds.h frequency.
// Despite its uselessness... the team wanted to
// implement it, so here it is.  It basically just
// tests that rds_start() fails if the frequency
// is > 88 or < 108, but succeeds otherwise.

int
main( void ) {
    assert( !rds_start( 100.0, "src/sound.wav" ) );
    rds_stop();
    assert( rds_start( 80, "src/sound.wav" ) );
    assert( rds_start( 109, "src/sound.wav" ) );
    return 0;
}
