/*
    PiFmRds - FM/RDS transmitter for the Raspberry Pi
    Copyright (C) 2014 Christophe Jacquet, F8FTK
    
    See https://github.com/ChristopheJacquet/PiFmRds

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RDS_H
#define RDS_H


#include <stdint.h>
#include <stdbool.h>

typedef enum rds_pt {
    PT_NONE,
    PT_NEWS,
    PT_CURRENT_AFFAIRS,
    PT_INFORMATION,
    PT_SPORT,
    PT_EDUCATION,
    PT_DRAMA,
    PT_CULTURE,
    PT_SCIENCE,
    PT_VARIED,
    PT_POP_MUSIC,
    PT_ROCK_MUSIC,
    PT_EASY_LISTENING,
    PT_LIGHT_CLASSICAL,
    PT_SERIOUS_CLASSICAL,
    PT_OTHER_MUSIC,
    PT_WHEATHER,
    PT_FINANCE,
    PT_CHILDRENS_PROGRAMS,
    PT_SOCIAL_AFFAIRS,
    PT_RELIGION,
    PT_PHONE_IN,
    PT_TRAVEL,
    PT_LAISURE,
    PT_JAZZ_MUSIC,
    PT_COUNTRY_MUSIC,
    PT_NATIONAL_MUSIC,
    PT_OLDIES_MUSIC,
    PT_FOLK_MUSIC,
    PT_DOCUMENTARY,
    PT_ALARM_TEST,
    PT_ALARM
} rds_pt;

void get_rds_samples(float *buffer, int count);
void set_rds_pi(uint16_t pi_code);
void set_rds_ps(char const *ps);
void set_rds_pty(rds_pt pty);
void set_rds_rt(char const *rt);
void set_rds_ta(bool ta);
void set_rds_tp(bool tp);


#endif /* RDS_H */
