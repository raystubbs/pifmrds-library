#ifndef pi_fm_rds_h
#define pi_fm_rds_h

#ifdef __cplusplus
extern "C" {
#endif

#include "rds.h"

// Start the transmission thread on frequency `freq`
// and audio file `audio_file`.  Returns 0 on success.
// May throw fatal errors for unexpected conditions.
int rds_start( float freq, char const* audio_file );

// Stop the transmission thread.
int rds_stop( void );

#ifdef __cplusplus
}
#endif

#endif
